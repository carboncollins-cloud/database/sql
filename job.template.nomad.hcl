job "database-sql" {
  name        = "Database SQL (MySQL)"
  type        = "service"
  region      = "se"
  datacenters = ["soc"]
  namespace   = "c3-database"

  priority = 60

  group "mysql" {
    count = 1

    consul {}

    network {
      mode = "bridge"
    }

    volume "mysql-data" {
      type      = "host"
      read_only = false
      source    = "mysql-data"
    }

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    service {
      provider = "consul"
      name     = "mysql"
      port     = "3306"
      task     = "mysql"

      connect {
        sidecar_service {}
      }

      tags = [
        "internal-proxy.enable=true",
        "internal-proxy.consulcatalog.connect=true",
        "internal-proxy.tcp.routers.mysql.rule=HostSNI(`*`)",
        "internal-proxy.tcp.routers.mysql.entryPoints=mysql"
      ]
    }

    task "mysql" {  
      driver = "docker"
      leader = true

      constraint {
        attribute = "${attr.cpu.arch}"
        value     = "amd64"
      }

      volume_mount {
        volume      = "mysql-data"
        destination = "/var/lib/mysql"
        read_only   = false
      }

      vault {
        role = "service-mysql"
      }

      resources {
        cpu    = 1000
        memory = 1024
      }

      config {
        image = "mysql:8.0.35"
      }

      env {
        TZ = "Europe/Stockholm"
      }

      template {
        data = <<EOH
        {{ with secret "c3kv/data/datacenter/soc/database/mysql" }}
          MYSQL_ROOT_PASSWORD={{ index .Data.data "rootPassword" }}
        {{ end }}
        EOH

        destination = "secrets/mysql.env"
        env = true
      }
    }
  }

  reschedule {
    delay          = "10s"
    delay_function = "exponential"
    max_delay      = "10m"
    unlimited      = true
  }

  update {
    health_check      = "checks"
    min_healthy_time  = "10s"
    healthy_deadline  = "10m"
    progress_deadline = "15m"
    auto_revert       = true
  }

  meta {
    gitSha      = "[[ .gitSha ]]"
    gitBranch   = "[[ .gitBranch ]]"
    pipelineId  = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId   = "[[ .projectId ]]"
    projectUrl  = "[[ .projectUrl ]]"
    statefull   = "true"
  }
}
